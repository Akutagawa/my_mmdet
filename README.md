# 概要
このレポジトリはmmdetection(https://github.com/open-mmlab/mmdetection) を拡張するレポジトリです.  
元のレポジトリからの変更された差分のファイルだけがレポジトリに含まれます.
